<p align="center">
  <a href="https://gitlab.com/odrassam/static-kemalangkuy">
    <img src="https://drive.google.com/file/d/1S6sWzH-tQgOCVfwD0QsRYv365F8l57qs/view?usp=sharing" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Sciotrip</h3>

  <p align="center">
    <a href="https://rdh-kemalangkuy.netlify.app/">View Demo</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
Sciotrip is advertisment website that provide various information for you when 
traveling to Malang city.

### Built With

This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.
* HTML
* SCSS
* JS
* Materialize Css


<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/odrassam/static-kemalangkuy
   ```







